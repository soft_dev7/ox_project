/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.ox_project;

import java.util.Scanner;

/**
 *
 * @author KHANOMMECOM
 */
public class OX_project {

    public static void showBoard(char[][] boardGame) {
        for (char[] row : boardGame) {
            for (char b : row) {
                System.out.print(b + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Welcome to OX Game!!");

        char[][] boardGame = new char[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                boardGame[i][j] = '-';
            }
        }

        boolean startGame = true;
        boolean endGame = false;
        int row = 0;
        int col = 0;

        while (!endGame) {
            showBoard(boardGame);

            char symbol = ' ';

            if (startGame) {
                symbol = 'O';
                System.out.println("Turn O");
            } else {
                symbol = 'X';
                System.out.println("Turn X");
            }

            while (true) {
                System.out.println("Please input row and col :");
                row = kb.nextInt();
                col = kb.nextInt();
                
                if(row <0 || col<0 || row>=3 || col>=3){
                    System.out.println("!!!This position is off the bounds "
                            + "of the board! Try again!!!");
                } else if(boardGame[row][col] != '-'){
                    System.out.println("!!!Someone has already made a "
                            + "move at this position! Try again!!!");
                }else {
                    break;
                }
               
                
            }boardGame[row][col] = symbol;

            if (checkWin(boardGame) == 'O') {
                showBoard(boardGame);
                System.out.println(">>>> O WIN <<<<");
                endGame = true;
            } else if (checkWin(boardGame) == 'X') {
                showBoard(boardGame);
                System.out.println(">>>> X WIN <<<<");
                endGame = true;
            } else {
                if (checkDraw(boardGame)) {
                    showBoard(boardGame);
                    System.out.println(">>>> DRAW!! <<<<");
                    endGame = true;
                } else {
                    startGame = !startGame;
                }
            }
        }
    }

    public static char checkWin(char[][] boardGame) {
        // checkRow
        for (int i = 0; i < 3; i++) {
            if (boardGame[i][0] == boardGame[i][1] && boardGame[i][1] == boardGame[i][2] && boardGame[i][0] != '-') {
                return boardGame[i][0];
            }
        }// checkCol
        for (int j = 0; j < 3; j++) {
            if (boardGame[0][j] == boardGame[1][j] && boardGame[1][j] == boardGame[2][j] && boardGame[0][j] != '-') {
                return boardGame[0][0];
            }
        }// left diagonal
        if (boardGame[0][0] == boardGame[1][1] && boardGame[1][1] == boardGame[2][2] && boardGame[0][0] != '-') {
            return boardGame[0][0];
        }// right diagonal
        if (boardGame[2][0] == boardGame[1][1] && boardGame[1][1] == boardGame[0][2] && boardGame[2][0] != '-') {
            return boardGame[2][0];
        }
        return ' ';
    }

    private static boolean checkDraw(char[][] boardGame) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (boardGame[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    
    
}
